﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class UserRole
    {
        public int Id { get; set; }
        public string Role { get; set; }
        public ICollection<User> Users { get; set; }

    }
}
