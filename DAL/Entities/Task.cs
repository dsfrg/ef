﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace DAL.Entities
{
    public class Task
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public TaskState State { get; set; }

        public Nullable<int> ProjectId { get; set; }
        public Project Project { get; set; }

        public int PerformerId { get; set; }
        public User Performer { get; set; }
    }
}
