﻿using AutoMapper;
using DAL.Context;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace DAL.Repository
{
    public class TeamRepository : IRepository<Team>
    {
        private readonly InitialDataContext _context;
        public TeamRepository(InitialDataContext context)
        {
            _context = context;
        }
        public void Add(Team item)
        {
            if (item is null)
                throw new ArgumentNullException("item");

            var team = _context.Teams.FirstOrDefault(x => x.Id == item.Id);
            if (team != null)
            {
                var nextIndex = _context.Teams.OrderBy(x => x.Id).Last().Id;
                item.Id = ++nextIndex;
            }

            _context.Teams.Add(item);
            _context.SaveChanges();
        }

        public void AddRange(IEnumerable<Team> range)
        {
            if (range is null)
                throw new ArgumentNullException("range");

            foreach (var item in range)
            {
                if (_context.Teams.Any(x => x.Id == item.Id))
                    throw new ArgumentException("range");
            }

            _context.Teams.AddRange(range);
            _context.SaveChanges();
        }

        public void Delete(Team item)
        {
            if (item is null)
                throw new ArgumentNullException("item");

            _context.Teams.Remove(item);
            _context.SaveChanges();
        }

        public void DeleteById(int id)
        {
            var team = _context.Teams.FirstOrDefault(x => x.Id == id);
            if (team == null)
                throw new ArgumentException("id");

            _context.Teams.Remove(team);
            _context.SaveChanges();
        }

        public Team GetItem(int id)
        {
            var team = _context.Teams.FirstOrDefault(x => x.Id == id);
            if (team == null)
                throw new ArgumentException("id");

            return team;
        }

        public IEnumerable<Team> GetItems()
        {
            return _context.Teams;
        }

        public IQueryable<Team> GetItemsQueryable()
        {
            return _context.Teams;
        }

        public void Update(Team item)
        {
            var team = _context.Teams.FirstOrDefault(x => x.Id == item.Id);
            if (team == null)
                throw new ArgumentException("item");
            _context.Teams.Update(item);
            _context.SaveChanges();
        }
    }
}
