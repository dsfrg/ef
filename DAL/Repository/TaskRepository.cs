﻿using AutoMapper;
using DAL.Context;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Repository
{
    public class TaskRepository : IRepository<Task>
    {
        private readonly InitialDataContext _context;
        public TaskRepository(InitialDataContext context)
        {
            _context = context;
        }
        public void Add(Task item)
        {
            if (item is null)
                throw new ArgumentNullException("item");

            var task = _context.Tasks.FirstOrDefault(x => x.Id == item.Id);
            if (task != null)
            {
                var nextIndex = _context.Tasks.OrderBy(x => x.Id).Last().Id;
                item.Id = ++nextIndex;
            }

            _context.Tasks.Add(item);
            _context.SaveChanges();
        }

        public void AddRange(IEnumerable<Task> range)
        {
            if (range is null)
                throw new ArgumentNullException("range");

            foreach (var item in range)
            {
                if (_context.Tasks.Any(x => x.Id == item.Id))
                    throw new ArgumentException("range");
            }
            _context.Tasks.AddRange(range);
            _context.SaveChanges();
        }

        public void Delete(Task item)
        {
            if (item is null)
                throw new ArgumentNullException("item");

            _context.Tasks.Remove(item);
            _context.SaveChanges();
        }

        public void DeleteById(int id)
        {
            var task = _context.Tasks.FirstOrDefault(x => x.Id == id);
            if (task == null)
                throw new ArgumentException("id");

            _context.Tasks.Remove(task);
            _context.SaveChanges();
        }

        public Task GetItem(int id)
        {
            var task = _context.Tasks.FirstOrDefault(x => x.Id == id);
            if (task == null)
                throw new ArgumentException("id");

            return task;
        }

        public IQueryable<Task> GetItemsQueryable()
        {
            return _context.Tasks;
        }

        public void Update(Task item)
        {
            if (item is null)
                throw new ArgumentNullException("item");

            var task = _context.Tasks.FirstOrDefault(x => x.Id == item.Id);
            if (task is null)
                throw new ArgumentException("item");
            _context.Tasks.Update(item);
            _context.SaveChanges();
        }

        public IEnumerable<Task> GetItems()
        {
            return _context.Tasks;
        }
    }
}
