﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DAL.Context
{
    public static class InitialDataContextExtension
    {
        public static string ReadDataFromJson(string path)
        {
            using (StreamReader reader = new StreamReader(path))
            {
                return reader.ReadToEnd();
            }
        }
        public static void Seed(this ModelBuilder modelBuilder)
        {

            var teams = new List<Team>(JsonConvert.DeserializeObject<IEnumerable<Team>>(ReadDataFromJson(@"..\Common\StarterData\Teams.json")));
            var users = new List<User>(JsonConvert.DeserializeObject<IEnumerable<User>>(ReadDataFromJson(@"..\Common\StarterData\Users.json")));
            var projects = new List<Project>(JsonConvert.DeserializeObject<IEnumerable<Project>>(ReadDataFromJson(@"..\Common\StarterData\Projects.json")));
            var tasks = new List<Task>(JsonConvert.DeserializeObject<IEnumerable<Task>>(ReadDataFromJson(@"..\Common\StarterData\Tasks.json")));

            var random = new Random();
            foreach (var user in users)
            {
                user.RoleId = random.Next(1,5);
            }
            var taskStateModels = new List<TaskStateModel>()
            { 
                new TaskStateModel()
                {
                    Id = 1,
                    Value = "Created"
                },
                new TaskStateModel()
                {
                    Id = 2,
                    Value = "Started"
                },

                new TaskStateModel()
                {
                    Id = 3,
                    Value = "Finished"
                },
                new TaskStateModel()
                {
                    Id = 4,
                    Value = "Canceled"
                }

            };
            var roles = new List<UserRole>()
            {
                new UserRole()
                {
                    Id = 1,
                    Role = "Developer"
                },

                new UserRole()
                {
                    Id = 2,
                    Role = "QA"
                },

                new UserRole()
                {
                    Id = 3,
                    Role = "DevOps"
                },

                new UserRole()
                {
                    Id = 4,
                    Role = "PM"
                }
            };

            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Task>().HasData(tasks);
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<TaskStateModel>().HasData(taskStateModels);
            modelBuilder.Entity<UserRole>().HasData(roles);
        }
    }
}
