﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs
{
    public enum TaskStateDto
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}
