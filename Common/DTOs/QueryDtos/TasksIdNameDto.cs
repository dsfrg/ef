﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.QueryDtos
{
    public class TasksIdNameDto
    {
        public int Id { get; set; }
        public string Name { get; set; }

    }
}
