﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.QueryDtos
{
    public class ProjectToTaskCountStructureDto
    {
        public ProjectDto Project { get; set; }
        public int CountOfTasks { get; set; }
    }
}
