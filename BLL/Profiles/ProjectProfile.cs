﻿using AutoMapper;
using Common.DTOs;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Profiles
{
    public sealed class ProjectProfile: Profile
    {
        public ProjectProfile()
        {

            CreateMap<Project, ProjectDto>().ReverseMap();
            CreateMap<Project, HierarchyData>().ReverseMap();
            CreateMap<ProjectDto, HierarchyData>().ReverseMap();
            CreateMap<Project, HierarchyDataDto>().ReverseMap();
            CreateMap<ProjectDto, HierarchyDataDto>().ReverseMap();
        }
    }
}
