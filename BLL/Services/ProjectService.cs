﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Common.DTOs;
using DAL.Entities;
using DAL.Repository;

namespace BLL.Services
{
    public class ProjectService
    {
        private readonly IRepository<Project> _projectRepo;
        private readonly IMapper _mapper;

        public ProjectService(IRepository<Project> repo, IMapper mapper)
        {
            _projectRepo = repo;
            _mapper = mapper;
        }

        public void AddProject(ProjectDto item)
        {
            _projectRepo.Add(_mapper.Map<Project>(item));
        }

        public void AddRangeOfProjects(IEnumerable<ProjectDto> range)
        {
            _projectRepo.AddRange(_mapper.Map<IEnumerable<Project>>(range));
        }

        public void Delete(ProjectDto item)
        {
            _projectRepo.Delete(_mapper.Map<Project>(item));
        }

        public void DeleteById(int id)
        {
            _projectRepo.DeleteById(id);
        }

        public ProjectDto GetProject(int id)
        {
            return _mapper.Map<ProjectDto>(_projectRepo.GetItem(id));
        }

        public IEnumerable<ProjectDto> GetProjects()
        {
            return _mapper.Map<IEnumerable<ProjectDto>>(_projectRepo.GetItems());
        }

        public void Update(ProjectDto item)
        {
            _projectRepo.Update(_mapper.Map<Project>(item));
        }
    }
}
