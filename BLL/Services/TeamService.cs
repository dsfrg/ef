﻿using AutoMapper;
using Common.DTOs;
using DAL.Entities;
using DAL.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Services
{
    public class TeamService
    {
        private readonly IRepository<Team> _teamRepo;
        private readonly IMapper _mapper;

        public TeamService(IRepository<Team> repo, IMapper mapper)
        {
            _teamRepo = repo;
            _mapper = mapper;
        }

        public void AddTeam(TeamDto item)
        {
            _teamRepo.Add(_mapper.Map<Team>(item));
        }

        public void AddRangeOfTeams(IEnumerable<TeamDto> range)
        {
            _teamRepo.AddRange(_mapper.Map<IEnumerable<Team>>(range));
        }

        public void Delete(TeamDto item)
        {
            _teamRepo.Delete(_mapper.Map<Team>(item));
        }

        public void DeleteById(int id)
        {
            _teamRepo.DeleteById(id);
        }

        public TeamDto GetTeam(int id)
        {
            return _mapper.Map<TeamDto>(_teamRepo.GetItem(id));
        }

        public IEnumerable<TeamDto> GetTeams()
        {
            return _mapper.Map<IEnumerable<TeamDto>>(_teamRepo.GetItems());
        }

        public void Update(TeamDto item)
        {
            _teamRepo.Update(_mapper.Map<Team>(item));
        }
    }
}
