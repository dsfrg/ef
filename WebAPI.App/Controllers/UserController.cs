﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Services;
using Common.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserService _userService;

        public UserController(UserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<UserDto>> GetAllUsers()
        {
            return Ok(_userService.GetUsers());
        }

        [HttpGet("{id}")]
        public ActionResult<UserDto> GetSingleUser(int id)
        {
            return Ok(_userService.GetUser(id));
        }


        [HttpPost]
        public IActionResult AddSingleUser(UserDto user)
        {
            _userService.AddUser(user);
            return Ok();
        }

        [HttpPost("range")]
        public IActionResult AddUsers(IEnumerable<UserDto> users)
        {
            _userService.AddRangeOfUsers(users);
            return Ok();
        }

        [HttpDelete]
        public IActionResult DeleteUser(UserDto user)
        {
            _userService.Delete(user);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteUser(int id)
        {
            _userService.DeleteById(id);
            return NoContent();
        }


        [HttpPut]
        public IActionResult ChangeUser(UserDto user)
        {
            _userService.Update(user);
            return Ok();
        }
    }
}
