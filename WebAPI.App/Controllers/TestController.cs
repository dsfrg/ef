﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Context;
using DAL.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace WebAPI.App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly InitialDataContext _context;

        public TestController(InitialDataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var a = _context.Teams.Include(x => x.Users);
            return Ok(a);
        }
    }
}
