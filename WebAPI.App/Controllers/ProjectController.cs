﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Services;
using Common.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly ProjectService _projectService;

        public ProjectController(ProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ProjectDto>> GetAllProjects()
        {
            return Ok(_projectService.GetProjects());
        }

        [HttpGet("{id}")]
        public ActionResult<ProjectDto> GetSingleProject(int id)
        {
            return Ok(_projectService.GetProject(id));
        }


        [HttpPost]
        public IActionResult AddSingleProject(ProjectDto project)
        {
            _projectService.AddProject(project);
            return Ok();
        }

        [HttpPost("range")]
        public IActionResult AddProjects(IEnumerable<ProjectDto> projects)
        {
            _projectService.AddRangeOfProjects(projects);
            return Ok();
        }

        [HttpDelete]
        public IActionResult DeleteProject(ProjectDto project)
        {
            _projectService.Delete(project);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteProjectById(int id)
        {
            _projectService.DeleteById(id);
            return NoContent();
        }


        [HttpPut]
        public IActionResult ChangeProject(ProjectDto project)
        {
            _projectService.Update(project);
            return Ok();
        }
    }
}
